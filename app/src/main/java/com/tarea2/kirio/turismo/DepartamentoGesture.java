package com.tarea2.kirio.turismo;
import android.app.Activity;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;

public class DepartamentoGesture  extends Activity implements GestureOverlayView.OnGesturePerformedListener {
    //Creamos el componente para relacionarlo con el XML
//Y el gesture library para rescatar el archivo que hemos
//creado anteriormente
    private GestureOverlayView gesture;
    private GestureLibrary gLibrary;
    private static int info;
    private int num;
    //En este caso solos nos interesa el gesto que más se parezca
            /*<item>San Salvador</item>
        <item> Santa Ana </item>
        <item> Sonsonate </item>
        <item> Ahuachapan </item>
        <item> Chalatenango </item>
        <item> La Libertad </item>no
        <item> Cuscatlan </item>no
        <item> Cabañas </item>............no
        <item> San Vicente </item>no
        <item> Morazan </item>no
        <item> San Miguel </item>no
        <item> La Union </item>no
        <item> Usulutan </item>no
        <item> La Paz </item>*/
    String[]activities={"SanSalvador","SantaAna","SonSonate","Ahuachapan","Chalatenango","LaLibertad","Cuscatlan","Cabanas","SanVicente",
            "Morazan","SanMiguel","LaUnion","Usulutan","LaPaz"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_departamento_gesture);

        //Relacionamos con el XML
        gesture = (GestureOverlayView)findViewById(R.id.gestureOverlayView1);
        //Le añadimos el listener
        gesture.addOnGesturePerformedListener(this);
        //Creamos la carpeta res/raw y añadimos el archivo gestures
        //añadimos el raw al gLibrary
        gLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        //lo cargamos...
        gLibrary.load();

    }
    //Al obtener algun gesto...



    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        //añadimos los diferentes resultados obtenidos
        //ya comparados con nuestro archivo y ordenados de mas fiable a menos
        ArrayList<Prediction> predictions = gLibrary.recognize(gesture);
        String comando = predictions.get(0).name;
        //Si a encontraod algun resultado
        if (predictions.size() > 0) {


            if(comando.equals("San Salvador")){
                Toast.makeText(this, "San Salvador",Toast.LENGTH_LONG).show();
                num=0;
                accion(num);

            } else if(comando.equals("santa ana")){
                Toast.makeText(this, "Santa Ana",Toast.LENGTH_LONG).show();
                num=1;
                accion(num);
            } else if(comando.equals("sonsonate")){
                Toast.makeText(this, "Sonsonate",Toast.LENGTH_LONG).show();
                num=2;
                accion(num);
            } else if(comando.equals("ahuachapan")){
                Toast.makeText(this, "Ahuachapan",Toast.LENGTH_LONG).show();
                num=3;
                accion(num);
            }
            else if(comando.equals("chalatenango")){
                Toast.makeText(this, "Chalatenango",Toast.LENGTH_LONG).show();
                num=4;
                accion(num);
            }else if(comando.equals("la libertad")){
                Toast.makeText(this, "La Libertad",Toast.LENGTH_LONG).show();
                num=5;
                accion(num);
            }
            else if(comando.equals("cuscatlan")){
                Toast.makeText(this, "Cuscatlan",Toast.LENGTH_LONG).show();
                num=6;
                accion(num);
            }
            else if(comando.equals("Cabanas")){
                Toast.makeText(this, "Cabanas",Toast.LENGTH_LONG).show();
                num=7;
                accion(num);
            }
            else if(comando.equals("san vicente")){
                Toast.makeText(this, "San Vicente",Toast.LENGTH_LONG).show();
                num=8;
                accion(num);
            }
            else if(comando.equals("Morazan")){
                Toast.makeText(this, "Morazan",Toast.LENGTH_LONG).show();
                num=9;
                accion(num);
            }
            else if(comando.equals("san miguel")){
                Toast.makeText(this, "San Miguel",Toast.LENGTH_LONG).show();
                num=10;
                accion(num);
            }
            else if(comando.equals("La Union")){
                Toast.makeText(this, "La Union",Toast.LENGTH_LONG).show();
                num=11;
                accion(num);
            }
            else if(comando.equals("Usulutan")){
                Toast.makeText(this, "Usulutan",Toast.LENGTH_LONG).show();
                num=12;
                accion(num);
            }
            else if(comando.equals("la paz")){
                Toast.makeText(this, "La Paz",Toast.LENGTH_LONG).show();
                num=13;
                accion(num);
            }


        }

    }



    public void accion(int n){
        String  nombreValue=activities[n];
        Class<?> clase;{
            try {
                clase = Class.forName("com.tarea2.kirio.turismo."+nombreValue);
                Intent intent = new Intent(DepartamentoGesture.this,clase);
                startActivity(intent);
            }
            catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } }

    }
}
