package com.tarea2.kirio.turismo;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {
    ViewPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    public String[] mDepartamento;

    //String[] titulo = getResources().getStringArray(R.array.categorias);


    protected static Integer[] mImageIds = {
            R.drawable.sansalvador_principal,
            R.drawable.santaana_principal,
            R.drawable.sonso_principal,
            R.drawable.ahua_principal,
            R.drawable.chalate_principal,
            R.drawable.lalibertad_principal,
            R.drawable.cuscatlan_principal,
            R.drawable.cabanas_principal,
            R.drawable.sanvicente_principal,
            R.drawable.morazan_principal,
            R.drawable.sanmiguel_principal,
            R.drawable.launion_principal,
            R.drawable.usulutan_principal,
            R.drawable.lapaz_principal,


    };

    ListView lista;
    String[] menu = {
            "Busqueda por gestos",
            "Elvator Pitch","Realizar Un consulta con voz","Animaciones","3D"

    };
    String[] activities={"DepartamentoGesture","ElevatorPitch"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.listViewHome);
        ArrayAdapter adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, menu);
        lista.setAdapter(adaptador);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != 3) {
                    nexActivity(activities[position]);


                }

            }
        });
        //FIN LISTVIEW

        //VIEWPAGER
        mDepartamento = getResources().getStringArray(R.array.departamentos);

        mSectionsPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);

        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(0,mDepartamento[0], mImageIds[0]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(1,mDepartamento[1], mImageIds[1]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(2,mDepartamento[2], mImageIds[2]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(3,mDepartamento[3], mImageIds[3]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(4,mDepartamento[4], mImageIds[4]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(5,mDepartamento[5], mImageIds[5]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(6,mDepartamento[6], mImageIds[6]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(7,mDepartamento[7], mImageIds[7]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(8,mDepartamento[8], mImageIds[8]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(9,mDepartamento[9], mImageIds[9]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(10,mDepartamento[10], mImageIds[10]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(11,mDepartamento[11], mImageIds[11]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(12,mDepartamento[12], mImageIds[12]));
        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(13,mDepartamento[13], mImageIds[13]));

        mViewPager.setAdapter(mSectionsPagerAdapter);
        //FIN VIEWPAGER

    }
    public void nexActivity(String layount){
        try {

            Class<?> clase = Class.forName("com.tarea2.kirio.turismo." + layount);
            Intent intent = new Intent(getApplicationContext(), clase);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            Toast.makeText(this, "ClassNotFount!!!", Toast.LENGTH_LONG).show();
        }

    }
    /****************** VIEWPAGER *********************/
    public class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments; //acá voy a guardar los fragments

        //constructor
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<Fragment>();
        }

        @Override
        public Fragment getItem(int position) {
            //return PlaceholderFragment.newInstance(position + 1);
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            //return 3;
            return this.fragments.size();
        }

        public void addFragment(Fragment xfragment){
            this.fragments.add(xfragment);
        }
    }
    /****************** FIN VIEWPAGER *********************/
    /****************** FRAGMENTOS *********************/
    public static class Fragmentos extends Fragment {

        private static final String CURRENT_VIEWVAPER ="currentviewpager";
        private static final String NOMBRE_DEPARTAMENTOS = "departamento";
        private static final String IMAGEVIEW = "image";

        private int currentViewPager;
        private String nombre_departamentos;
        private int image;

        public static Fragmentos newInstance(int currentViewPager, String departamentoNombre, int image) {

            Fragmentos fragment = new Fragmentos();   //instanciamos un nuevo fragment

            Bundle args = new Bundle();                                 //guardamos los parametros
            args.putInt(CURRENT_VIEWVAPER, currentViewPager);
            args.putString(NOMBRE_DEPARTAMENTOS, departamentoNombre);
            args.putInt(IMAGEVIEW, image);
            fragment.setArguments(args);
            fragment.setRetainInstance(true);     //agrego para que no se pierda los valores de la instancia
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            //cuando crea una instancia de tipo PlaceholderFragment
            //si lo enviamos parametros, guarda esos
            //si no le envio nada, toma el color gris y un número aleatroio
            if(getArguments() != null){
                this.currentViewPager = getArguments().getInt(CURRENT_VIEWVAPER);
                this.nombre_departamentos = getArguments().getString(NOMBRE_DEPARTAMENTOS);
                this.image = getArguments().getInt(IMAGEVIEW);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_departamento, container, false);

            TextView tv_circuito = (TextView) rootView.findViewById(R.id.tv_departamento);
            tv_circuito.setText(nombre_departamentos);

            ImageView frg_image = (ImageView) rootView.findViewById(R.id.frg_imageView);
            frg_image.setImageResource(image);
            frg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), Departamento.class);
                    i.putExtra("currentViewPager", currentViewPager);
                    i.putExtra("nombreDepartamentos", nombre_departamentos);
                    startActivity(i);
                    //overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            });
            return rootView;
        }
    }
    /****************** FIN FRAGMENTOS *********************/

}
