package com.tarea2.kirio.turismo;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
public class Departamento extends ActionBarActivity {

    int[] imagenSanSalvador = {
            R.drawable.sansalvador_catedral,// imagenes con tamanio 115X77 pixeles q se son la prevista al contenido completo
            R.drawable.sansalvador_lagoilopango,
            R.drawable.sansalvador_palacionacional,
            R.drawable.sansalvador_saburohirao,
            R.drawable.sansalvador_soo

    };

    int[] imagenSantaAna= {
            R.drawable.santaana_coatepeque,
            R.drawable.santaana_apuzunga,
            R.drawable.santaana_sihuatehuacan,
            R.drawable.santaana_cerroverde,
            R.drawable.santaana_montecristo

    };

    int[] imagenSonsonate = {
            R.drawable.sonsonate_atecozol,
            R.drawable.sonsonate_loscobanos,
            R.drawable.sonsonate_volcanizalco,
            R.drawable.sonsonate_puertoacajutla,
            R.drawable.sonsonate_bocanadesanjuan
    };

    int[] imagenAhuachapan = {
            R.drawable.ahuachapan_imposible,
            R.drawable.ahuachapan_barradesantiago,
            R.drawable.ahuachapan_rutadelasflores,
            R.drawable.ahuachapan_termalesdesantateresa,
            R.drawable.ahuachapan_asuncion
    };

    int[] imagenChalate = {
            R.drawable.chalatenango_elpital,
            R.drawable.chalatenango_lapalma,
            R.drawable.chalatenango_miramundo,
            R.drawable.chalatenango_aguafria,
            R.drawable.chalatenango_riosumpul
    };

    int[] imagenLaLibertad = {
            R.drawable.lalibertad_diego1,
            R.drawable.lalibertad_obispo1,
            R.drawable.lalibertad_lapaz1,
            R.drawable.lalibertad_majahual1
    };

    int[] imagenCabanas = {
            R.drawable.cabanas_tejute1,
            R.drawable.cabanas_dolores1,
            R.drawable.cabaniaas_cerrogrande,
            R.drawable.cabanias_cinquera

    };

    int[] imagenSanVicente= {
            R.drawable.sanvicente_apastepeque,
            R.drawable.sanvicente_infiernillos,
            R.drawable.sanvicente_mapulapa,
            R.drawable.sanvicente_sansebastian


    };

    int[] imagenCuscatlan = {
            R.drawable.cuscatlan_cerrolacolima,
            R.drawable.cuscatlan_ciudadvieja,
            R.drawable.cuscatlan_lagosuchitlan,
            R.drawable.cuscatlan_laspavas

    };

    int[] imagenLaPaz = {
            R.drawable.lapaz_costasol1,
            R.drawable.lapaz_ichanmichen1,
            R.drawable.lapaz_parque1,
            R.drawable.lapaz_volcan1
    };

    int[] imagenMorazan = {
            R.drawable.morazan_llanomuerto1,
            R.drawable.morazan_cerroperquin1,
            R.drawable.morazan_bailaderodiablo1,
            R.drawable.morazan_museo1,
            R.drawable.morazan_guatajiagua1,
    };

    int[] imagenLaUnion = {
            R.drawable.launion_parquefamilia1,
            R.drawable.launion_playasnegras1,
            R.drawable.launion_playitas1,
            R.drawable.launion_playatamarindo1,
            R.drawable.launion_golfo1,

    };
    int[] imagenUsulutan = {
            R.drawable.usulutan_playaespino1,
            R.drawable.usulutan_bahiajiquilisco1,
            R.drawable.usulutan_playalapuntilla1,
            R.drawable.usulutan_estanzuelas1,
            R.drawable.usulutan_lagunaalegria1,
    };

    int[] imagenSanMiguel = {
            R.drawable.sanmiguel_playaelcuco1,
            R.drawable.sanmiguel_bascatedral1,
            R.drawable.sanmiguel_teatro1,
            R.drawable.sanmiguel_elturicentro1,
            R.drawable.sanmiguel_moncagua1,
    };
    
    String[] titulo;
    String[] contenido;

    private ListView lista;
    ListViewAdapter adapter;

    int currentViewPager;
    String nombreDepartamento;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departamento);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);  //ir atras

        Bundle extras = getIntent().getExtras();
        currentViewPager = extras.getInt("currentViewPager");
        nombreDepartamento = extras.getString("nombreDepartamento");
        Log.i("kirio", "currentViewPager: " + currentViewPager);



        actionBar.setTitle(nombreDepartamento);

        lista = (ListView) findViewById(R.id.listView_listarDepartamento);

        switch (currentViewPager){
            case 0: //SanSalvador
                titulo = getResources().getStringArray(R.array.DepdeSanSalvador_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanSalvador_contenido);
                adapter = new ListViewAdapter(this, imagenSanSalvador, titulo, contenido);
                break;
            case 1: //Santa Ana
                titulo = getResources().getStringArray(R.array.DepdeSantaAna_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSantaAna_contenido);
                adapter = new ListViewAdapter(this, imagenSantaAna, titulo, contenido);
                break;

            case 2: //Sonsonate
                titulo = getResources().getStringArray(R.array.DepdeSonsonate_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSonsonate_contenido);
                adapter = new ListViewAdapter(this, imagenSonsonate, titulo, contenido);
                break;

            case 3: //Ahuachapan
                titulo = getResources().getStringArray(R.array.DepdeAhuchapan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeAhuchapan_contenido);
                adapter = new ListViewAdapter(this, imagenAhuachapan, titulo, contenido);
                break;

            case 4: //Chalatenango
                titulo = getResources().getStringArray(R.array.DepdeChalatenango_titulo);
                contenido = getResources().getStringArray(R.array.DepdeChalatenango_contenido);
                adapter = new ListViewAdapter(this, imagenChalate, titulo, contenido);
                break;

            case 5: //La Libertad
                titulo = getResources().getStringArray(R.array.DepdeLaLibertad_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaLibertad_contenido);
                adapter = new ListViewAdapter(this, imagenLaLibertad, titulo, contenido);
                break;

            case 6: //Cuscatlan
                titulo = getResources().getStringArray(R.array.DepdeCuscatlan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeCuscatlan_contenido);
                adapter = new ListViewAdapter(this, imagenCuscatlan, titulo, contenido);
                break;


            case 7: //Cabañas
                titulo = getResources().getStringArray(R.array.DepdeCabanas_titulo);
                contenido = getResources().getStringArray(R.array.DepdeCabanas_contenido);
                adapter = new ListViewAdapter(this, imagenCabanas, titulo, contenido);
                break;
            case 8: //San Vicente
                titulo = getResources().getStringArray(R.array.DepdeSanVicente_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanVicente_contenido);
                adapter = new ListViewAdapter(this, imagenSanVicente, titulo, contenido);
                break;

                /*=========================Zona Oriental======================================*/
            case 9: //Morazan
                titulo = getResources().getStringArray(R.array.DepdeMorazan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeMorazan_contenido);
                adapter = new ListViewAdapter(this, imagenMorazan, titulo, contenido);
                break;

            case 10: //SanMiguel
                titulo = getResources().getStringArray(R.array.DepdeSanMiguel_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanMiguel_contenido);
                adapter = new ListViewAdapter(this, imagenSanMiguel, titulo, contenido);
                break;

            case 11: //LaUnion
                titulo = getResources().getStringArray(R.array.DepdeLaUnion_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaUnion_contenido);
                adapter = new ListViewAdapter(this, imagenLaUnion, titulo, contenido);
                break;

            case 12: //Usulutan
                titulo = getResources().getStringArray(R.array.DepdeUsulutan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeUsulutan_contenido);
                adapter = new ListViewAdapter(this, imagenUsulutan, titulo, contenido);
                break;
            /*=========================Zona Oriental======================================*/
            case 13: //la paz
                titulo = getResources().getStringArray(R.array.DepdeLaPaz_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaPaz_contenido);
                adapter = new ListViewAdapter(this, imagenLaPaz, titulo, contenido);
                break;




            default:
                Toast.makeText(getApplicationContext(), "no esta cargado, pronto lo estará", Toast.LENGTH_SHORT).show();
        }
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DepartamentoLista.class);
                i.putExtra("iddepartamento",currentViewPager);
                i.putExtra("position", position);
                i.putExtra("nombreDepartamento", nombreDepartamento);
                i.putExtra("nombreSubDepartamento", titulo[position]);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }
        });

    }
    public class ListViewAdapter extends BaseAdapter {
        // Declare Variables
        // Declare Variables
        Context context;
        int[] imagenes;
        String[] titulos;
        String[] contenido;
        LayoutInflater inflater;

        public ListViewAdapter(Context context, int[] imagenes, String[] titulos, String[] contenido ) {
            this.context = context;
            this.imagenes = imagenes;
            this.titulos = titulos;
            this.contenido = contenido;
        }

        @Override
        public int getCount() {
            return titulos.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            // Declare Variables
            ImageView imgImg;
            TextView txtTitle;
            TextView txtContenido;


            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View itemView = inflater.inflate(R.layout.single_post_departamento, parent, false);

            // Locate the TextViews in listview_item.xml

 imgImg = (ImageView) itemView.findViewById(R.id.imagen_single_post_departamento);
            txtTitle = (TextView) itemView.findViewById(R.id.tv_titulo_single_post_departamento);
            txtContenido = (TextView) itemView.findViewById(R.id.tv_contenido_single_post_departamento);

            // Capture position and set to the TextViews
            imgImg.setImageResource(imagenes[position]);
            txtTitle.setText(titulos[position]);
            txtContenido.setText(contenido[position]);
            return itemView;
        }
    }
}
