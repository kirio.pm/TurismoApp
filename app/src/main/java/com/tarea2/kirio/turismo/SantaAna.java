package com.tarea2.kirio.turismo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SantaAna extends ActionBarActivity {

    ViewPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    public String[] mDepartamento;

    //String[] titulo = getResources().getStringArray(R.array.categorias);


    protected static Integer[] mImageIds = {
            R.drawable.santaana_principal


    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_san_salvador);



        //VIEWPAGER
        mDepartamento = getResources().getStringArray(R.array.departamentos);

        mSectionsPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);

        mSectionsPagerAdapter.addFragment(Fragmentos.newInstance(1, mDepartamento[1], mImageIds[0]));


        mViewPager.setAdapter(mSectionsPagerAdapter);
        //FIN VIEWPAGER

    }
    /****************** VIEWPAGER *********************/
    public class ViewPagerAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments; //acá voy a guardar los fragments

        //constructor
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<Fragment>();
        }

        @Override
        public Fragment getItem(int position) {
            //return PlaceholderFragment.newInstance(position + 1);
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            //return 3;
            return this.fragments.size();
        }

        public void addFragment(Fragment xfragment){
            this.fragments.add(xfragment);
        }
    }
    /****************** FIN VIEWPAGER *********************/
    /****************** FRAGMENTOS *********************/
    public static class Fragmentos extends Fragment {

        private static final String CURRENT_VIEWVAPER ="currentviewpager";
        private static final String NOMBRE_DEPARTAMENTOS = "departamento";
        private static final String IMAGEVIEW = "image";

        private int currentViewPager;
        private String nombre_departamentos;
        private int image;

        public static Fragmentos newInstance(int currentViewPager, String departamentoNombre, int image) {

            Fragmentos fragment = new Fragmentos();   //instanciamos un nuevo fragment

            Bundle args = new Bundle();                                 //guardamos los parametros
            args.putInt(CURRENT_VIEWVAPER, currentViewPager);
            args.putString(NOMBRE_DEPARTAMENTOS, departamentoNombre);
            args.putInt(IMAGEVIEW, image);
            fragment.setArguments(args);
            fragment.setRetainInstance(true);     //agrego para que no se pierda los valores de la instancia
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            //cuando crea una instancia de tipo PlaceholderFragment
            //si lo enviamos parametros, guarda esos
            //si no le envio nada, toma el color gris y un número aleatroio
            if(getArguments() != null){
                this.currentViewPager = getArguments().getInt(CURRENT_VIEWVAPER);
                this.nombre_departamentos = getArguments().getString(NOMBRE_DEPARTAMENTOS);
                this.image = getArguments().getInt(IMAGEVIEW);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_departamento, container, false);

            TextView tv_circuito = (TextView) rootView.findViewById(R.id.tv_departamento);
            tv_circuito.setText(nombre_departamentos);

            ImageView frg_image = (ImageView) rootView.findViewById(R.id.frg_imageView);
            frg_image.setImageResource(image);
            frg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), Departamento.class);
                    i.putExtra("currentViewPager", currentViewPager);
                    i.putExtra("nombreDepartamentos", nombre_departamentos);
                    startActivity(i);
                    //overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            });
            return rootView;
        }

    }
}
