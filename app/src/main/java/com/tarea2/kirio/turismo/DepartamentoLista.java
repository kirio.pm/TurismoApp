package com.tarea2.kirio.turismo;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DepartamentoLista extends ActionBarActivity {
    private ImageView imgImagen;
    private TextView txtTitulo, txtContenido;
    String[] titulo;
    String[] contenido;
    int[] imagenSanSalvador = {
            R.drawable.iglesia_metroploitan, // imagenes con mayor resolucion
            R.drawable.ilopango_sansalvador,
            R.drawable.palacio_nacional,
            R.drawable.saburo_03,
            R.drawable.soo_2

    };

    int[] imagenSantaAna= {
            R.drawable.santaana_coatepeque2,
            R.drawable.santaana_apuzunga2,
            R.drawable.santaana_sihuatehuacan2,
            R.drawable.santaana_cerroverde2,
            R.drawable.santaana_montecristo2

    };

    int[] imagenSonsonate = {
            R.drawable.sonsonate_atecozol2,
            R.drawable.sonsonate_loscobanos2,
            R.drawable.sonsonate_volcanizalco2,
            R.drawable.sonsonate_puertoacajutla2,
            R.drawable.sonsonate_bocanadesanjuan2

    };

    int[] imagenAhuachapan = {
            R.drawable.ahuachapan_imposible2,
            R.drawable.ahuachapan_barradesantiago2,
            R.drawable.ahuachapan_rutadelasflores2,
            R.drawable.ahuachapan_termalesdesantateresa2,
            R.drawable.ahuachapan_asuncion2


    };

    int[] imagenChalate = {
            R.drawable.chalatenango_elpital2,
            R.drawable.chalatenango_lapalma2,
            R.drawable.chalatenango_miramundo2,
            R.drawable.chalatenango_aguafria2,
            R.drawable.chalatenango_riosumpul2

    };

    int[] imagenLaLibertad = {
            R.drawable.lalibertad_diego2,
            R.drawable.lalibertad_obispo2,
            R.drawable.lalibertad_lapaz2,
            R.drawable.lalibertad_majahual2
                      
    };

    int[] imagenCabanas = {
            R.drawable.cabanas_tejute2,
            R.drawable.cabanas_dolores2,
            R.drawable.cabanias_cerrogrande2,
            R.drawable.cabanias_cinquera2
            

    };

    int[] imagenSanVicente= {
            R.drawable.sanvicente_apastepeque2,
            R.drawable.sanvicente_infiernillos2,
            R.drawable.sanvicente_mapulapa2,
            R.drawable.sanvicente_sansebastian2
    };

    int[] imagenCuscatlan = {
            R.drawable.cuscatlan_cerrolacolima2,
            R.drawable.cuscatlan_ciudadvieja2,
            R.drawable.cuscatlan_lagosuchitlan2,
            R.drawable.cuscatlan_laspavas2

    };

    int[] imagenLaPaz = {
            R.drawable.lapaz_costasol2,
            R.drawable.lapaz_ichanmichen2,
            R.drawable.lapaz_parque2,
            R.drawable.lapaz_volcan2
    };

    int[] imagenMorazan = {
            R.drawable.morazan_llanomuerto2,
            R.drawable.morazan_cerroperquin2,
            R.drawable.morazan_bailaderodiablo2,
            R.drawable.morazan_museo2,
            R.drawable.morazan_guatajiagua2,
    };

    int[] imagenLaUnion = {
            R.drawable.launion_parquefamilia2,
            R.drawable.launion_playasnegras2,
            R.drawable.launion_playitas2,
            R.drawable.launion_playatamarindo2,
            R.drawable.launion_golfo2,

    };
    int[] imagenUsulutan = {
            R.drawable.usulutan_playaespino2,
            R.drawable.usulutan_bahiajiquilisco2,
            R.drawable.usulutan_playalapuntilla2,
            R.drawable.usulutan_estanzuelas2,
            R.drawable.usulutan_lagunaalegria2,
    };

    int[] imagenSanMiguel = {
            R.drawable.sanmiguel_playaelcuco2,
            R.drawable.sanmiguel_bascatedral2,
            R.drawable.sanmiguel_teatro2,
            R.drawable.sanmiguel_elturicentro2,
            R.drawable.sanmiguel_moncagua2,
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departamento_lista);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        int iddepartamento = extras.getInt("iddepartamento");
        final int position = extras.getInt("position");
        String nombreDepartamento = extras.getString("nombreDepartamento");
        String nombreSubDepartamento = extras.getString("nombreSubDepartamento");

        /**INDICAR TITULO Y SUBTITULO**/
        actionBar.setTitle(nombreDepartamento);
        actionBar.setSubtitle(nombreSubDepartamento);

        txtTitulo = (TextView) findViewById(R.id.tv_titulo_listarundepartamento);
        txtContenido = (TextView) findViewById(R.id.tv_contenido_listarundepartamento);
        imgImagen = (ImageView) findViewById(R.id.iv_imagen_listarundepartamento);

        switch (iddepartamento){
            case 0: //SanSalvador
                titulo = getResources().getStringArray(R.array.DepdeSanSalvador_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanSalvador_contenido_completo);
                imgImagen.setImageResource(imagenSanSalvador[position]);

                break;
            case 1: //Santa Ana
                titulo = getResources().getStringArray(R.array.DepdeSantaAna_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSantaAna_contenido_completo);
                imgImagen.setImageResource(imagenSantaAna[position]);
                break;

            case 2: //Sonsonate
                titulo = getResources().getStringArray(R.array.DepdeSonsonate_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSonsonate_contenido_completo);
                imgImagen.setImageResource(imagenSonsonate[position]);
                break;

            case 3: //Ahuachapan
                titulo = getResources().getStringArray(R.array.DepdeAhuchapan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeAhuchapan_contenido_completo);
                imgImagen.setImageResource(imagenAhuachapan[position]);
                break;

            case 4: //Chalatenango
                titulo = getResources().getStringArray(R.array.DepdeChalatenango_titulo);
                contenido = getResources().getStringArray(R.array.DepdeChalatenango_contenido_completo);
                imgImagen.setImageResource(imagenChalate[position]);
                break;

             case 5: //La Libertad
                titulo = getResources().getStringArray(R.array.DepdeLaLibertad_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaLibertad_contenido_completo);
                imgImagen.setImageResource(imagenLaLibertad[position]);
                break;

            case 6: //Cuscatlan
                titulo = getResources().getStringArray(R.array.DepdeCuscatlan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeCuscatlan_contenido_completo);
                imgImagen.setImageResource(imagenCuscatlan[position]);
                break;

            case 7: //Cabanias
                titulo = getResources().getStringArray(R.array.DepdeCabanas_titulo);
                contenido = getResources().getStringArray(R.array.DepdeCabanas_contenido_completo);
                imgImagen.setImageResource(imagenCabanas[position]);
                break;


            case 8: //San Vicente
                titulo = getResources().getStringArray(R.array.DepdeSanVicente_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanVicente_contenido_completo);
                imgImagen.setImageResource(imagenSanVicente[position]);
                break;
                /*=========================Zona Oriental======================================*/
            case 9: //Morazan
                titulo = getResources().getStringArray(R.array.DepdeMorazan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeMorazan_contenido_completo);
                imgImagen.setImageResource(imagenMorazan[position]);
                break;

            case 10: //San miguel
                titulo = getResources().getStringArray(R.array.DepdeSanMiguel_titulo);
                contenido = getResources().getStringArray(R.array.DepdeSanMiguel_contenido_completo);
                imgImagen.setImageResource(imagenSanMiguel[position]);
                break;
            case 11: //La union
                titulo = getResources().getStringArray(R.array.DepdeLaUnion_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaUnion_contenido_completo);
                imgImagen.setImageResource(imagenLaUnion[position]);
                break;

            case 12: //usulutan
                titulo = getResources().getStringArray(R.array.DepdeUsulutan_titulo);
                contenido = getResources().getStringArray(R.array.DepdeUsulutan_contenido_completo);
                imgImagen.setImageResource(imagenUsulutan[position]);
                break;
            /*=========================Zona Oriental======================================*/
            case 13: //la paz
                titulo = getResources().getStringArray(R.array.DepdeLaPaz_titulo);
                contenido = getResources().getStringArray(R.array.DepdeLaPaz_contenido_completo);
                imgImagen.setImageResource(imagenLaPaz[position]);
                break;


            default:
                Toast.makeText(getApplicationContext(), "no esta cargado, pronto lo estará", Toast.LENGTH_SHORT).show();
        }
        txtTitulo.setText(titulo[position]);
        txtContenido.setText(contenido[position]);

    }
}
